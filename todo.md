## To do

- [x] Add helm s3 plugin to Runner docker image
- [ ] Create releases file / other solution....
- [x] Create secrets (Variables) for S3 integration to push the charts.
- [x] Publish the chart to S3
- [ ] Deploy chart (release name) - install or upgrade - param....


## Thoughts

Every update/release of component service, let's say ___msa-api___ will require auto deploy to lab/staging according to release name ....  
How to know which release should be updated? prompt?


## Prerequisites to run the helm s3 plugin
- python 2.7
- aws cli
- aws credentials (from variables)
- k8s config (from variables)
- s3 bucket to use (should be predefined)
