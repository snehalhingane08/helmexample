## Helm chart for MSA-DEMO-APP solution

This repo contains helm charts for `msa-demo-app` solution, which contains:
* ___msa-api___ microservice (depends on redis chart)
* ___msa-pinger___ microservice
* ___msa-poller___ microservice

Each time some changes is done to this chart - new deployment will be triggered.

## The Goal

No, it's not about [the book](https://en.wikipedia.org/wiki/The_Goal_(novel))...  
The goal of this effort is to show __GitLab CI/CD__ possibilities during implementation of [___Docker Compose___](https://github.com/shelleg/msa-demo-app/tree/gitlab-ci-anatoly/.compose) based application deployment conversion to K8S Helm chart based deployment.

## What will not be covered

 - Changes made in components for K8S deployment compatibility and further maintenance (docker images, software structure etc.)
 - Helm implementation details (covered [here](#))

## Vision

I'm concentrating on solution (umbrella) chart deployment and not on specific microservice deployment approach.  
Specific microservice might be used in more than one solution, msa-demo-app is on of solutions.  
New version of ___msa_demo-app___ version should not affect any solution (should be tested first in that solution etc.)  
Once specific microservice version is ready (it's pipeline ends with specific docker image in registry and helm chart in S3 charts repo), [requirements.yaml](msa-demo-app/requirements.yaml) should be edited and specific chart version should be changed to the relevant one.  
Example: we have new ___msa-api___ service ver __1.0.0-rc.3__
```yaml
dependencies:
- name: msa-api
  repository: '@tikal'
  version: 1.0.0-rc.3

- name: msa-pinger
  repository: '@tikal'
  version: 1.0.0-rc.2

- name: msa-poller
  repository: '@tikal'
  version: 1.0.0-rc.2
```
Only after this action (commit/push etc.), ___msa-demo-app___ solution will be prepared and deployed (pre-followed by changing the solution chart version in [Chart.yaml](msa-demo-app/Chart.yaml))

## Assumptions

In order to show CI/CD aspects, some "functional" aspects were simplified (hardcoded) just to show the concept.  
Feel free to clone this repo/create your own branch and make your improvements!!!  

- release name of __msa-demo-app__ in our cluster will be ___krusty-krab___ (there is [releases.yaml](releases.yaml) which can be used to play with release names and target envs).
- K8S namespace where this app will be deployed should exist in K8S cluster its anme is currently hardcoded to be __msa-demo-app__ in [.gitlab-ci.yml](.gitlab-ci.yml):

```yaml
...
script:
- helm upgrade -i krusty-krab tikal/${CI_PROJECT_NAME} --version ${CHART_VER} --namespace msa-demo-app
```

### CI/CD Implementation

My choose to use SaaS GitLab CI/CD was driven by the need to have fast and simple CI/CD platform.  
In case of GitLab SaaS solution, I have 4 components in one place:
- Source Control (git flavor)
- Docker Registry
- CI/CD pipeline infrastructure
- Artifacts repo

I've implemented CI/CD using __public gitlab runners__ - free platform which allows to realize linux os, docker (or docker in docker) based pipeline (pipeline which will be executed on linux machine, docker image or will generate docker image for further use).  
It has no language or DSL dependency, you can use whatever you want, just prepare your runner with appropriate tools (shell scripts, make, ansible, docker, capistrano, fabric, you-name-it.....).

All what you need is to be familiar with basic concepts of building [.gitlab-ci.yml](https://docs.gitlab.com/ee/ci/yaml/README.html) file.  
After reading about it few minutes - you'll be able to implement your basic/simple pipeline, after few hours - more advanced pipeline - after few days - powerful and flexible pipeline.....

__TLDR;__ take a look at my [.gitlab-ci.yml](.gitlab-ci.yml) to check what happens on `git push` action....
